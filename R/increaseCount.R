#
#  C O N S T R U C T I V I S T__ __
#   _____ ____   ____   /  |/  // /
#  / ___// __ \ / __ \ / /|_/ // /
# / /__ / /_/ // / / // /  / // /___
# \___/ \____//_/ /_//_/  /_//_____/
#  M A C H I N E   L E A R N I N G
#
#
# A Project by
# Thomas Schmid | UNIVERSITÄT LEIPZIG
# www.constructivist.ml
#
# Code Author: Dennis Carrer
# Licence: GNU GPLv3
#
#

#' increase the count
#'
#' This function increase the count on the level
#'
#' @param countArray is an Array with a count for each level
#' @param level ist the number of lvele where ur learning Block is at the moment, default 0
#'
#'
#' @details
#' Test
#'
#'
#'


increaseCount <- function(countArray,level){
  countArray[[level+1]] <- as.numeric(countArray[[level+1]]) + 1
  return(countArray)
}
