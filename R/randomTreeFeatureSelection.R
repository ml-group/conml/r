#
#  C O N S T R U C T I V I S T__ __
#   _____ ____   ____   /  |/  // /
#  / ___// __ \ / __ \ / /|_/ // /
# / /__ / /_/ // / / // /  / // /___
# \___/ \____//_/ /_//_/  /_//_____/
#  M A C H I N E   L E A R N I N G
#
#
# A Project by
# Thomas Schmid | UNIVERSITÄT LEIPZIG
# www.constructivist.ml
#
# Code Author: Dennis Carrer
# Licence: GNU GPLv3
#
#

#' Feature Ranking by Random Forest Importance (randomForest)
#'
#' This function is for randomTree
#'
#' @param testblock a Dataset
#' @param maxFeatures is the maximum number of Features that you want after Feature Reduction
#' @param maxModelReduction is a boolean for if we do a maxModelReduction
#'
#'
#' @details
#' Test
#'
#'
#'




rT_FeatureSelection <- function(testblock, maxFeatures, maxModelReduction){

  # FEATURE RANKING BY RANDOM FOREST IMPORTANCE (randomForest)

  target <- testblock$metadata
  features <- testblock$data

  # test predictions for specific cluster
  no_of_features <- ncol(features)

  if (no_of_features < maxFeatures){
    maxFeatures <- no_of_features
  }

  # select training/ test data for
  index <- 1:nrow(testblock$data)
  no_of_testcases <- round( nrow(testblock$data)/3, digits=0)
  test_index <- sample( index, no_of_testcases )

  test_data    <- features[ test_index,]
  test_target <- target[ test_index,]
  train_data   <- features[-test_index,]
  train_target <- target[-test_index,]

  # set up parallel execution
  cl <- parallel::makeCluster(32)
  doSNOW::registerDoSNOW(cl)

  rf <- foreach(y=seq(10), .combine=randomForest::combine, .packages = "randomForest" ) %dopar% {
    lrf <- randomForest::randomForest(as.factor(train_target$Z) ~., data=train_data, ntree=50, norm.votes=FALSE)
  }
  parallel::stopCluster(cl)

  features_importance <- randomForest::importance(rf)
  features_row_names  <- as.numeric( rownames( data.table::as.data.table( features_importance)))
  features_tmp        <- cbind( features_importance, features_row_names )
  features_ranking    <- as.vector(features_tmp[ order(features_tmp[,1], decreasing=TRUE), 2 ])

  min_error   <- 1
  min_error_i <- maxFeatures




  #maxModelReduction
  if(maxModelReduction == "FALSE"){
    selected_subset <- sort( features_ranking[ 1:min_error_i ])
    reducedBlock <- dplyr::select(testblock$data, selected_subset)
    reducednames <- names(reducedBlock)
    testblock$data <- dplyr::select(testblock$data, reducednames)

  }else if(maxModelReduction == "TRUE"){

    for (x in 2:maxFeatures) {

      subset <- features_ranking[1:x]

      test_data1    <- testblock$data[ test_index, subset ]
      train_data1   <- testblock$data[-test_index, subset ]

      cl <- parallel::makeCluster(32)
      doSNOW::registerDoSNOW(cl)

      rf <- foreach(y=seq(10), .combine=randomForest::combine, .packages = "randomForest" ) %dopar% {
        lrf <- randomForest::randomForest( as.factor(train_target$Z) ~., data=train_data1, ntree=50, norm.votes=FALSE)
      }

      parallel::stopCluster(cl)


      pr <- stats::predict ( rf, test_data1 )

      cm       <- as.matrix(table(Target = test_target$Z, Predicted = pr))
      accuracy <- sum(diag(cm)) / no_of_testcases
      error    <- 1-accuracy

      if ( error < min_error) {
        min_error   <- error
        min_error_i <- x
      }
    }
    selected_subset <- sort( features_ranking[ 1:min_error_i ])
    reducedBlock <- dplyr::select(testblock$data, selected_subset)
    reducednames <- names(reducedBlock)
    testblock$data <- dplyr::select(testblock$data, reducednames)

  }

  print("Feature Reduction via randomTree")
  subject <- testblock$conPath
  print(paste("Subject: ", subject))
  print(paste("Features: ", ncol(testblock$data)))
  print(paste("Samples: ", nrow(testblock$data)))

  return (testblock)

}
