#
#  C O N S T R U C T I V I S T__ __
#   _____ ____   ____   /  |/  // /
#  / ___// __ \ / __ \ / /|_/ // /
# / /__ / /_/ // / / // /  / // /___
# \___/ \____//_/ /_//_/  /_//_____/
#  M A C H I N E   L E A R N I N G
#
#
# A Project by
# Thomas Schmid | UNIVERSITÄT LEIPZIG
# www.constructivist.ml
#
# Code Author: Dennis Carrer
# Licence: GNU GPLv3
#
#

#' countTIntersection
#'
#' This function count the T Intersections
#'
#' @param candidate is your candidate fro Deconstruction
#' @param old_Model is your Model from KnowledgeDomain
#'
#'
#' @details
#' Test
#'
#'
#'



countTIntersection <- function(candidate,old_Model){
  count <- 0
  for( i in candidate$metadata$T){
    for(j in old_Model$metadata$T){
      if(i == j){
        count <- count+1
      }
    }
  }
  return(count)
}

