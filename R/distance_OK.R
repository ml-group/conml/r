#
#  C O N S T R U C T I V I S T__ __
#   _____ ____   ____   /  |/  // /
#  / ___// __ \ / __ \ / /|_/ // /
# / /__ / /_/ // / / // /  / // /___
# \___/ \____//_/ /_//_/  /_//_____/
#  M A C H I N E   L E A R N I N G
#
#
# A Project by
# Thomas Schmid | UNIVERSITÄT LEIPZIG
# www.constructivist.ml
#
# Code Author: Dennis Carrer
# Licence: GNU GPLv3
#
#

#' checks distance
#'
#' This function checks the distance
#'
#' @param distanceMeasure is the measured distance
#' @param deconMaxDistanceT is the max distance
#'
#'
#' @details
#' Test
#'
#'
#'
#'




distance_OK <-function(distanceMeasure,deconMaxDistanceT){
  if(distanceMeasure<=deconMaxDistanceT){
    return(TRUE)
  }else{
    return(FALSE)
  }
}


