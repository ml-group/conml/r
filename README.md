# Constructivist Machine Learning (conML) 

## Dependencies
- R >= 4.0.0

## Installation
1. Start a new R console.
```bash
$ R
```
2. Install `devtools` from CRAN.
```r
> install.packages("devtools")
```
3. Install conML from Git.
```r
> install_git("https://git.informatik.uni-leipzig.de/ml-group/conml/r.git", ref="master")
```

See [reference](https://remotes.r-lib.org/reference/install_git.html) for more information about `ìnstall_git()`.

## Documentation

You'll find the documentation as quick docs bundled in the package (see https://r-pkgs.org/man.html
for more information).

## Quick Start

1. First create a Knowledgedomain with input `maxLevel (int)`:
```r
kn <- createKnowledgeDomain(4)
```

2. There are 2 ways of using conML  

The __default mode__:
```r
conML <- conMl(data,knowledgedomain)
```

or with __all variables__:
```r
conML <- conMl(Data, KnowledgeDomain, 
                domain = "conceptual",
                maxLevel = 5, 
                minBlockSize = 500, 
                cutOffSigma = 20, 
                gridSize = 256, 
                constructionMethod1 = "SOM",
                constructionMethod2 = "KMeans", 
                maxCategories = 5,    
                minClusterSize = 0.1,
                FilterMethod = "CFS", 
                EmbeddedMethod = "randomForest", 
                minFeatures = 2, 
                maxFeatures = 10, 
                maxFilterX = 350, 
                maxFilterY = 50000,
                maxModelReduction = "TRUE", 
                reconstructionMethod1 = "randomForest", 
                reconstructionMethod2 = "mlp", 
                minTestAccuracy = 0.8,
                RpropAlpha = 0, 
                RpropDeltaMax = 50.0, 
                RpropDeltaMin = 0.1, 
                minReliability = 0.8, 
                deconStrategy = "conservative", 
                deconMode = "maximal", 
                deconMaxDistanceT = 1, 
                tsFullTolerance = 0.1, 
                maxTimeConflict = 0.1)
```

## Examples

You'll find an usage example in [examples](./examples/).

## Parameters

- `maxModelReduction`: `TRUE` or `FALSE`
- `deconStrategy`: `conservative`,`integrative` or `opportunistic`
- `deconMode`: `maximal` or `minimal`

Parameters, which are limited in this version:  
- `domain`: only `conceptual`
- `constructionMethod1`: no other method
- `constructionMethod2` no other method
- `FilterMethod`: no other method
- `EmbeddedMethod`: no other method
- `reconstructionMethod1`: no other method
- `reconstructionMethod2`: no other method

### Plots 

To plot your knowledge domain, you can use:

```r
plot <- plotKnowledgeDomain(knowledgedomain = knowledgedomain)
```
